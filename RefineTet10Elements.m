function [tetNew,Pnew,Unew] = RefineTet10Elements(tet,P,markedElem,U)
    nnod = size(P,1);
    nele = size(tet,1);
    
    Pnew = NaN(9*nnod,3);
    tetNew = NaN(4*nele,10);
    Pnew(1:nnod,:) = P;
    tetNew(1:nele,:)=tet;
    refEle = false(4*nele,1);
    
    Unew = NaN(9*nnod,1);
    Unew(1:nnod) = U;
    
    cutEdge = zeros(8*nnod,3);        % cut edges
    nCut = 0;                         % number of cut edges
    nonConforming = true(8*nnod,1);   % flag of the non-conformity of edges
    
    while ~isempty(markedElem)        
        %% Compute edge lengths
        t = markedElem;
        % switch element nodes such that elem(t,1:2) is the longest edge of t
        allEdge = [tetNew(t,[1 2]); tetNew(t,[1 3]); tetNew(t,[1 4]); ...
                   tetNew(t,[2 3]); tetNew(t,[2 4]); tetNew(t,[3 4])];
        allEdgeLength = sum((P(allEdge(:,1),:)-P(allEdge(:,2),:)).^2,2);
        allEdgeLength = (1 + 0.1*rand(size(allEdge,1),1)).*allEdgeLength;
        elemEdgeLength = reshape(allEdgeLength,length(t),6);
        [~,idx] = max(elemEdgeLength,[],2);

        %% Reorder the vertices of elem
        tetNew(t((idx==2)),:) = tetNew(t((idx==2)),[3 1 2 4 7 5 6 10 8 9]);
        tetNew(t((idx==3)),:) = tetNew(t((idx==3)),[1 4 2 3 8 9 5 7 10 6]);
        tetNew(t((idx==4)),:) = tetNew(t((idx==4)),[2 3 1 4 6 7 5 9 10 8]);
        tetNew(t((idx==5)),:) = tetNew(t((idx==5)),[2 4 3 1 9 10 6 5 8 7]);
        tetNew(t((idx==6)),:) = tetNew(t((idx==6)),[4 3 2 1 10 6 9 8 7 5]);

        %% All edges
%         AllEdges = [tet(:,[1,2,5]);
%                     tet(:,[2,3,6]);
%                     tet(:,[1,3,7]);
%                     tet(:,[1,4,8]);
%                     tet(:,[2,4,9]);
%                     tet(:,[3,4,10
%                     ])];
%         % find(sparse) eliminates possible duplications
%         [~,ai] = unique(sort(AllEdges(:,1:2),2),'rows');
%         AllEdges = AllEdges(ai,:);
%         AllEdges(:,1:2) = sort(AllEdges(:,1:2),2);

        %%
%         Faces3 = [tetNew(:,[1,4,2]); tetNew(:,[1,3,4]); tetNew(:,[3,2,4]); tetNew(:,[1,2,3]);];
%         Faces3 = Faces3(any(~isnan(Faces3),2),:);
%         xfigure()
%         patch('Faces',Faces3,'Vertices',Pnew,'FaceColor','none'); axis equal off tight; hold on; view(3)
%         Faces3 = [tetNew(markedElem,[1,4,2]); tetNew(markedElem,[1,3,4]); tetNew(markedElem,[3,2,4]); tetNew(markedElem,[1,2,3]);];
%         patch('Faces',Faces3,'Vertices',Pnew,'FaceColor','c','FaceAlpha',0.2);
%         PP = Pnew(any(~isnan(Pnew),2),:);
%         iv = tetNew(markedElem,:)';

%         PP = PP(iv(:),:);
%         text(PP(:,1), PP(:,2), PP(:,3), cellstr( num2str(iv(:)) ), 'BackgroundColor','w');

        %%
        p1 = tetNew(markedElem,1);    
        p2 = tetNew(markedElem,2);
        p3 = tetNew(markedElem,3); 
        p4 = tetNew(markedElem,4);
        p5 = tetNew(markedElem,5);
        p6 = tetNew(markedElem,6);
        p7 = tetNew(markedElem,7);
        p8 = tetNew(markedElem,8);
        p9 = tetNew(markedElem,9);
        p10 = tetNew(markedElem,10);

        %% Find new cut edges and new nodes
        nMarked = length(markedElem);     % number of marked elements
        
        elemCutEdge = sort([p1 p2],2); % all new cut edges
        [elemCutEdgeUnique,ia] = unique(elemCutEdge,'rows','stable');
        nNew = size(elemCutEdgeUnique,1); % number of new cut edges

        % Indices of new cut edges already exist, and are stored in p5
        newCutEdge = nCut+1:nCut+nNew; % indices of new cut edges
        cutEdge(newCutEdge,1) = elemCutEdgeUnique(:,1);     % add cut edges
        cutEdge(newCutEdge,2) = elemCutEdgeUnique(:,2);     % cutEdge(:,1:2) two end nodes
        cutEdge(newCutEdge,3) = p5(ia)';   % cutEdge(:,3) middle point
        nCut = nCut + nNew;            % update number of cut edges

        %% 4 new internal points
        %New point coordinates
        allNewEdges = [ [p1,p5]; [p2,p5]; [p3,p5]; [p4,p5] ];
        allNewEdges = unique(allNewEdges,'rows', 'stable'); %unique edges
        
        %Check if allNewEdges exist in cutEdge, do not create duplicate
        %nodes!
        nv2v = sparse([cutEdge(1:nCut,3);cutEdge(1:nCut,3)],...
                      [cutEdge(1:nCut,1);cutEdge(1:nCut,2)],1,nnod,nnod);
        [~,j] = find(nv2v(:,allNewEdges(:,1)).*nv2v(:,allNewEdges(:,2)));
        allNewEdges(j,:) = []; %Duplicates
        
        
        nNewEdges = size(allNewEdges,1);
        newNodInds = nnod+1 : nnod+nNewEdges; %Creating new nodes
        Pnew(newNodInds, :) = ( Pnew(allNewEdges(:,1),:) + Pnew(allNewEdges(:,2),:) )/2;
        newCutEdge = nCut+1 : nCut+nNewEdges; %New cut edge indices
        cutEdge(newCutEdge,:) = [allNewEdges,newNodInds'];
        nCut = nCut+nNewEdges;
        nnod = nnod+nNewEdges;

        
        
        nv2v = sparse([cutEdge(1:nCut,3);cutEdge(1:nCut,3)],...
                          [cutEdge(1:nCut,1);cutEdge(1:nCut,2)],1,nnod,nnod);

        [i,j] = find(nv2v(:,p1).*nv2v(:,p5)); % find middle points locally               
        p15 = zeros(length(i),1);  
        p15(j) = i;

        [i,j] = find(nv2v(:,p2).*nv2v(:,p5)); % find middle points locally               
        p25 = zeros(length(i),1);  
        p25(j) = i;

        [i,j] = find(nv2v(:,p3).*nv2v(:,p5)); % find middle points locally               
        p35 = zeros(length(i),1);  
        p35(j) = i;

        [i,j] = find(nv2v(:,p4).*nv2v(:,p5)); % find middle points locally               
        p45 = zeros(length(i),1);  
        p45(j) = i;    

    %     clear nv2v elemCutEdge

        %% Bisect marked elements
        % new element connectivity
        newEle = [p4 p1 p3 p5 p8 p7 p10 p45 p15 p35;
                  p3 p2 p4 p5 p6 p9 p10 p35 p25 p45];
%         Faces3 = [newEle(:,[1,4,2]); newEle(:,[1,3,4]); newEle(:,[3,2,4]); newEle(:,[1,2,3]);];
    %     xfigure; axis equal;
%         patch('Faces',Faces3,'Vertices',Pnew,'FaceColor','r','FaceAlpha',0.2);
%         PP = Pnew(newNodInds,:);
%         text(PP(:,1), PP(:,2), PP(:,3), cellstr( num2str(newNodInds') ), 'BackgroundColor','y');

        %% Interpolating field U in new nodes
        for iPrentElement = 1:nMarked
            iPrentNodes = tetNew(iPrentElement,:);
            iNewChildNodes = unique(newEle(iPrentElement:nMarked:end,8:end));
            Xparent = Pnew(iPrentNodes,:); %Coordinates of parent element
            Xchild = Pnew(iNewChildNodes,:); 
            [fi,~,~,~,~] = Tet10Element.basescalar(Xparent, Xchild); %interpolant function
            iU = Unew(iPrentNodes);
            iUchild = fi*iU;
            Unew(iNewChildNodes) = iUchild;
        end

        %% Update tet element matrix
        tetNew(markedElem,:) = [p4 p1 p3 p5 p8 p7 p10 p45 p15 p35];
        newEleInds = nele+1:nele+nMarked;
        tetNew(newEleInds,:) = [p3 p2 p4 p5 p6 p9 p10 p35 p25 p45];
        nele = nele + nMarked;
        refEle([markedElem(:);newEleInds(:)]) = true;
    %     clear elemGeneration p1 p2 p3 p4 p5

%         for iel = 1:nele
%            iv =  tetNew(iel,:);
%            xm = mean(Pnew(iv,1)); ym = mean(Pnew(iv,2)); zm = mean(Pnew(iv,3));
%            text(xm, ym, zm, num2str(iel),'BackgroundColor','c')
%         end

        %% Find non-conforming elements
        checkEdge = find(nonConforming(1:nCut)); % check non-conforming edges
        isCheckNode = false(nnod,1);                
        isCheckNode(cutEdge(checkEdge,1)) = true; % check two end nodes of  
        isCheckNode(cutEdge(checkEdge,2)) = true; % non-conforming edges
        isCheckElem = isCheckNode(tetNew(1:nele,1)) | isCheckNode(tetNew(1:nele,2))...
                    | isCheckNode(tetNew(1:nele,3)) | isCheckNode(tetNew(1:nele,4))...
                    | isCheckNode(tetNew(1:nele,5)) | isCheckNode(tetNew(1:nele,6))...
                    | isCheckNode(tetNew(1:nele,7)) | isCheckNode(tetNew(1:nele,8))...
                    | isCheckNode(tetNew(1:nele,9)) | isCheckNode(tetNew(1:nele,10));
        checkElem = find(isCheckElem); % all elements containing checking nodes
        t2v = sparse(repmat(checkElem,10,1), tetNew(checkElem,1:10), 1, nele, nnod);
        [i,j] = find(t2v(:,cutEdge(checkEdge,1)).*t2v(:,cutEdge(checkEdge,2)));
        allCommonElements = unique(i); %All elements containing new nodes.
        markedElem = setdiff(allCommonElements, find(refEle));
        nonConforming(checkEdge) = false;
        nonConforming(checkEdge(j)) = true;
        
    end
    %% Clean up
    tetNew = tetNew(~any(isnan(tetNew),2),:);
    Pnew = Pnew(~any(isnan(Pnew),2),:);
    Unew = Unew(~any(isnan(Unew),2),:);
    
end