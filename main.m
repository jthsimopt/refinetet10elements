clear
clc
close all

%%
ne = 2;
R=0.46;

%% P2 Tet mesh
x0 = -1; x1 = 1;
y0 = -1; y1 = 1;
z0 = -1; z1 = 1;
nxe = ne; nye = ne; nze = ne;

mesh = Tet2Mesh(x0,x1,y0,y1,z0,z1,nxe,nye,nze);

%%
tet2 = mesh.T; P2 = mesh.P;
nele = size(tet2,1);
markedElem = [1:6];

%% Surface function
surfaceFunction = @(x,y,z) ((x-0).^2 + (y-0).^2 + (z-0).^2).^0.5-R; %Spehre
phi = surfaceFunction(mesh.P(:,1), mesh.P(:,2), mesh.P(:,3));


 %% P1 Mesh
 tet1 = tet2(:,1:4);
 P1 = P2(unique(tet1),:);
 Faces3 = [tet1(:,[1,4,2]);
     tet1(:,[1,3,4]);
     tet1(:,[3,2,4]);
     tet1(:,[1,2,3]);];
 xfigure(1);
 hp1 = patch('Faces',Faces3,'Vertices',P1,'FaceColor','none'); axis equal off tight; hold on;
 ht1 = text(P1(:,1), P1(:,2), P1(:,3), cellstr( num2str((1:size(P1,1))') ), 'BackgroundColor','w');

 %% Refine Elements
[tet2,P2,phi] = RefineTet10Elements(tet2,P2,markedElem,phi);
Faces3 = [tet2(:,[1,4,2]); tet2(:,[1,3,4]); tet2(:,[3,2,4]); tet2(:,[1,2,3]);];
xfigure(2);
patch('Faces',Faces3,'Vertices',P2,'FaceColor','none'); axis equal off tight; hold on; view(3)
plot3(P2(:,1), P2(:,2), P2(:,3), 'ko', 'MarkerFaceColor', 'k')
nele = size(tet2,1);
for iel = 1:nele
    iv =  tet2(iel,:);
    xm = mean(P2(iv,1)); ym = mean(P2(iv,2)); zm = mean(P2(iv,3));
    text(xm, ym, zm, num2str(iel),'BackgroundColor','c')
end
